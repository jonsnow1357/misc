#!/usr/bin/env python
################################################################################
import platform
import sys
import os
import unittest
################################################################################
class TestQuick(unittest.TestCase):

  #@unittest.skip("")
  def test_platform(self):
    print("== platform")
    print("   architecture: {}".format(list(platform.architecture())))
    print("       platform: {}".format(platform.platform()))
    print("          uname: {}".format(list(platform.uname())))
    print("         python: {}".format(platform.python_implementation()))
    print("python compiler: {}".format(platform.python_compiler()))
  ##############################################################################
  #@unittest.skip("")
  def test_sys(self):
    print("== sys")
    print("endianness: {}".format(sys.byteorder))
    print("  platform: {}".format(sys.platform))
    print("    python: {}".format(sys.version))
    print("-- sys.path")
    for v in sys.path:
      print("  {}".format(v))
  ##############################################################################
  #@unittest.skip("")
  def test_os(self):
    print("== os")
    try:
      print("      login: {}".format(os.getlogin()))
    except OSError:
      pass
    if(not sys.platform.startswith("win")):
      print("        uid: {}".format(os.getresuid()))
    for env in ("LANG", "PYTHONIOENCODING", "TERM", "USER"):
      if(env in os.environ.keys()):
        print("-- {}:".format(env))
        if(sys.platform.startswith("win")):
          print(os.environ[env].replace(";", "\n"))
        elif(sys.platform.startswith("linux")):
          print(os.environ[env].replace(":", "\n"))
################################################################################
if(__name__ == "__main__"):
  vLvl = 2

  tests = unittest.TestLoader().loadTestsFromTestCase(TestQuick)
  unittest.TextTestRunner(verbosity=vLvl).run(tests)
################################################################################
