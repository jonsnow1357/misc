#!/usr/bin/env python
################################################################################
import importlib
import unittest
################################################################################
class TestPyAuto(unittest.TestCase):

  #@unittest.skip("")
  def test_version(self):
    lstPyauto = ("pyauto_base", "pyauto_comm", "pyauto_hw", "pyauto_hwdesign",
                 "pyauto_lab", "pyauto_templates", "pyauto_tools")
    for m in lstPyauto:
      try:
        mod = importlib.import_module(m)
        print("{:>20} version: {}".format(m, mod.__version__))
      except ImportError:
        print("{:>20} NOT FOUND".format(m))
################################################################################
if(__name__ == "__main__"):
  vLvl = 2

  tests = unittest.TestLoader().loadTestsFromTestCase(TestPyAuto)
  unittest.TextTestRunner(verbosity=vLvl).run(tests)
################################################################################
