#!/usr/bin/env python
################################################################################
import os
import sys
import unittest
################################################################################
class TestEnv(unittest.TestCase):

  #@unittest.skip("")
  def test_env(self):
    for k in sorted(os.environ.keys()):
      print("{}\n  {}".format(k, os.environ[k]))
  ##############################################################################
  def test_path(self):
    if("PATH" in os.environ.keys()):
      print("\n\n-- PATH:")
      if(sys.platform.startswith("win")):
        lst = os.environ["PATH"].split(";")
      elif(sys.platform.startswith("linux")):
        lst = os.environ["PATH"].split(":")
      else:
        raise RuntimeError
      for f in lst:
        print("[{}] {}".format("y" if(os.path.exists(f)) else "n", f))
    '''
    if("PYTHONPATH" in os.environ.keys()):
      print("-- PYTHONPATH:")
      if(sys.platform.startswith("win")):
        print(os.environ["PYTHONPATH"].replace(";", "\n"))
      elif(sys.platform.startswith("linux")):
        print(os.environ["PYTHONPATH"].replace(":", "\n"))
    '''
################################################################################
if(__name__ == "__main__"):
  vLvl = 2

  tests = unittest.TestLoader().loadTestsFromTestCase(TestEnv)
  unittest.TextTestRunner(verbosity=vLvl).run(tests)
################################################################################
