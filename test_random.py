#!/usr/bin/env python
################################################################################
import random
import unittest
################################################################################
class TestRandom(unittest.TestCase):

  #@unittest.skip("")
  def test_random(self):
    val = random.randint(0, 2)
    if(val == 0):
      self.fail()
    elif(val == 1):
      self.skipTest("")
################################################################################
if(__name__ == "__main__"):
  vLvl = 2

  tests = unittest.TestLoader().loadTestsFromTestCase(TestRandom)
  unittest.TextTestRunner(verbosity=vLvl).run(tests)
################################################################################
